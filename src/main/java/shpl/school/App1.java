package shpl.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class App1 {
    private App1() {

    }

    private static final InputStream PATH_TO_PROPERTIES = App1.class.getClassLoader().getResourceAsStream("property.properties");
    private static final List<String> TYPESLIST = Arrays.asList("int", "double", "integer", "float", "byte", "short", "long");
    private static final Logger LOGGER = LoggerFactory.getLogger(App1.class);

    @SuppressWarnings("All")
    public static <T extends Number> void main(String[] args) {

        String type = System.getProperty("type");
        if (type == null) {
            type = "int";
        } else if (!TYPESLIST.contains(type.toLowerCase())) {
            LOGGER.info("type is not at base or is not correct ");
            return;
        }

        PropertiesReader props = new PropertiesReader(PATH_TO_PROPERTIES);
        Calculation solver = new Calculation(props.getMin(), props.getMax(), props.getIncrement());
        LOGGER.info("Starting calculations" + System.lineSeparator());
        T[][] result = solver.calculate(type);

        WriteCalculations.write(result);
        LOGGER.info("Program finished its work");
    }
}
