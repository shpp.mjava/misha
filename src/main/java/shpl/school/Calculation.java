package shpl.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Calculation {
    private static final Logger LOGGER = LoggerFactory.getLogger(Calculation.class);
    private final String minimum;
    private final String maximum;
    private final String increm;

    public Calculation(String min, String max, String increment) {
        this.minimum = min;
        this.maximum = max;
        this.increm = increment;
    }

    @SuppressWarnings("unchecked")
    public <T extends Number & Comparable<T>> T[][] calculate(String type) {
        T min = (T) TypeCorrector.getType(this.minimum, type);
        T max = (T) TypeCorrector.getType(this.maximum, type);
        T increment = (T) TypeCorrector.getType(this.increm, type);

        T[][] result;
        int arrayLength = 0;

        if (increment.equals(0)) {
            LOGGER.info("increment = 0");
            throw new IllegalArgumentException("increment is 0");
        }

        int i1 = 0;
        for (T i = min; compare(i, max, increment, type); i = OperationsGeneric.add(i, increment, type)) {
            arrayLength++;
        }
        result = (T[][]) new Number[arrayLength][arrayLength];
        LOGGER.info("Starting to calculate" + System.lineSeparator());
        for (T i = min; compare(i, max, increment, type); i = OperationsGeneric.add(i, increment, type), i1++) {
            int j1 = 0;
            for (T j = min; compare(j, max, increment, type); j = OperationsGeneric.add(j, increment, type), j1++) {
                result[i1][j1] = OperationsGeneric.multiply(i, j, type);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private <T extends Number & Comparable<T>> boolean compare(T i, T max, T increment, String type) {
        T zero = (T) TypeCorrector.getType("0", type);
        if (increment.compareTo(zero) > 0) {
            return (i.compareTo(max) <= 0);
        } else return (i.compareTo(max) >= 0);
    }
}
