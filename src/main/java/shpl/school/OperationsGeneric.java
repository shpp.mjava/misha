package shpl.school;

class OperationsGeneric {
    private static final String EXCEPTION_MESSAGE = "Unsupported type";

    private OperationsGeneric() {
    }

    @SuppressWarnings("unchecked")
    public static <T extends Number> T add(T a, T b, String type) {
        if ((type.equalsIgnoreCase("Integer") || type.equalsIgnoreCase("int")) && a instanceof Integer) {
            TypeCorrector.correction(String.valueOf(a.intValue() + b.intValue()), type);
            return (T) Integer.valueOf(a.intValue() + b.intValue());
        } else if (type.equalsIgnoreCase("double") && a instanceof Double) {
            return (T) Double.valueOf(a.doubleValue() + b.doubleValue());
        } else if (type.equalsIgnoreCase("Float") && a instanceof Float) {
            TypeCorrector.correction(String.valueOf(a.floatValue() + b.floatValue()), type);
            return (T) Float.valueOf(a.floatValue() + b.floatValue());
        } else if (type.equalsIgnoreCase("long") && a instanceof Long) {
            return (T) Long.valueOf(a.longValue() + b.longValue());
        } else if (type.equalsIgnoreCase("short") && a instanceof Short) {
            TypeCorrector.correction(String.valueOf(a.shortValue() + b.shortValue()), type);
            return (T) Short.valueOf((short) (a.shortValue() + b.shortValue()));
        } else if (type.equalsIgnoreCase("byte") && a instanceof Byte) {
            TypeCorrector.correction(String.valueOf(a.shortValue() + b.shortValue()), type);
            return (T) Byte.valueOf((byte) (a.byteValue() + b.byteValue()));
        } else {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Number> T multiply(T a, T b, String type) {
        if ((type.equalsIgnoreCase("Integer") || type.equalsIgnoreCase("int")) && a instanceof Integer) {
            TypeCorrector.correction(String.valueOf(a.intValue() * b.intValue()), type);
            return (T) Integer.valueOf(a.intValue() * b.intValue());
        } else if (type.equalsIgnoreCase("double") && a instanceof Double) {
            return (T) Double.valueOf(a.doubleValue() * b.doubleValue());
        } else if (type.equalsIgnoreCase("Float") && a instanceof Float) {
            TypeCorrector.correction(String.valueOf(a.floatValue() * b.floatValue()), type);
            return (T) Float.valueOf(a.floatValue() * b.floatValue());
        } else if (type.equalsIgnoreCase("long") && a instanceof Long) {
            return (T) Long.valueOf(a.longValue() * b.longValue());
        } else if (type.equalsIgnoreCase("short") && a instanceof Short) {
            TypeCorrector.correction(String.valueOf(a.shortValue() * b.shortValue()), type);
            return (T) Short.valueOf((short) (a.shortValue() * b.shortValue()));
        } else if (type.equalsIgnoreCase("byte") && a instanceof Byte) {
            TypeCorrector.correction(String.valueOf(a.byteValue() * b.byteValue()), type);
            return (T) Byte.valueOf((byte) (a.byteValue() * b.byteValue()));
        } else {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
    }

}
