package shpl.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WriteCalculations {
    private WriteCalculations() {
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(WriteCalculations.class);

    public static <T extends Number> void write(T[][] array) {
        LOGGER.info("Writing results" + System.lineSeparator());
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                LOGGER.info("{} ", array[i][j]);
            }
            LOGGER.info(System.lineSeparator());
        }
    }
}
