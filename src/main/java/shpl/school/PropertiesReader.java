package shpl.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertiesReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesReader.class);
    Properties properties = new Properties();

    public PropertiesReader(InputStream pathToFile) {
        LOGGER.info("Trying to read the properties" + System.lineSeparator());
        InputStreamReader isr;
        try {
            isr = new InputStreamReader(pathToFile, StandardCharsets.UTF_8);
            properties.load(isr);
            isr.close();
            LOGGER.info("properties was read" + System.lineSeparator());

        } catch (NullPointerException e) {
            LOGGER.error("Something wrong with the path to file or it does not exist");
            throw new IllegalArgumentException("Something wrong with file " + e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getMin() {
        return (String) properties.get("minimum");
    }

    public String getMax() {
        return (String) properties.get("maximum");
    }

    public String getIncrement() {
        return (String) properties.get("increment");
    }
}
