package shpl.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TypeCorrector {
    private static final Logger LOGGER = LoggerFactory.getLogger(TypeCorrector.class);
    private static final String EXCEPTION_MESSAGE = "Unsupported type";

    private TypeCorrector() {
    }

    public static Number getType(String variable, String type) {
        try {
            correction(variable, type);
            if ("double".equalsIgnoreCase(type)) {
                return Double.valueOf(variable);
            } else if ("int".equalsIgnoreCase(type) || "Integer".equalsIgnoreCase(type)) {
                return Integer.valueOf(variable);
            } else if ("float".equalsIgnoreCase(type)) {
                return Float.valueOf(variable);
            } else if ("long".equalsIgnoreCase(type)) {
                return Long.valueOf(variable);
            } else if ("Short".equalsIgnoreCase(type)) {
                return Short.valueOf(variable);
            } else if ("byte".equalsIgnoreCase(type)) {
                return Byte.valueOf(variable);
            }
        } catch (NumberFormatException e) {
            LOGGER.error("NumberFormatException " + e.getMessage());
            throw new NumberFormatException("Not correct numbers");
        }
        throw new IllegalArgumentException(EXCEPTION_MESSAGE);
    }


//    @SuppressWarnings("all")
    public static void correction(String variable, String type) {
        String message = "Out of range for {} for type {}";
        if ("byte".equalsIgnoreCase(type) &&
                (Double.parseDouble(variable) > Byte.MAX_VALUE || Double.parseDouble(variable) < Byte.MIN_VALUE)) {
            LOGGER.error(message, Double.valueOf(variable), type);
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
        if (("int".equalsIgnoreCase(type) || "integer".equalsIgnoreCase(type)) &&
                (Double.parseDouble(variable) > Integer.MAX_VALUE || Double.parseDouble(variable) < Integer.MIN_VALUE)) {
            LOGGER.error(message, Double.valueOf(variable), type);
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
        if ("short".equalsIgnoreCase(type) &&
                (Double.parseDouble(variable) > Short.MAX_VALUE || Double.parseDouble(variable) < Short.MIN_VALUE)) {
            LOGGER.error(message, Double.valueOf(variable), type);
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
        if ("float".equalsIgnoreCase(type) &&
                (Double.parseDouble(variable) > Float.MAX_VALUE || Math.abs(Double.parseDouble(variable)) < -Float.MAX_VALUE)) {
            LOGGER.error(message, Double.valueOf(variable), type);
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
    }

}