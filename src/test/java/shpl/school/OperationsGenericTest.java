package shpl.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class OperationsGenericTest {

    @Test
    void addInt() {
        int expectedResult = 2;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(1, 1, "int"));
    }

    @Test
    void addIntNotEquals() {
        int expectedResult = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(1, 1, "int"));
    }

    @Test
    void addIntLargeFirstLetterOfType() {
        int expectedResult = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(1, 2, "Int"));
    }

    @Test
    void addInteger() {
        int expectedResult = 2;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(1, 1, "Integer"));
    }

    @Test
    void addIntegerNotEquals() {
        int expectedResult = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(1, 1, "Integer"));
    }


    @Test
    void addByte() {
        Byte expectedResult = 2;
        byte a = 1;
        byte b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "Byte"));
    }

    @Test
    void addByteLittleFirstLetterOfType() {
        Byte expectedResult = 2;
        byte a = 1;
        byte b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "byte"));
    }

    @Test
    void addByteNotEquals() {
        Byte expectedResult = 3;
        byte a = 1;
        byte b = 1;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(a, b, "Byte"));
    }

    @Test
    void addShort() {
        Short expectedResult = 2;
        short a = 1;
        short b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "Short"));
    }

    @Test
    void addShortLittleFirstLetterOfType() {
        Short expectedResult = 2;
        short a = 1;
        short b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "short"));
    }

    @Test
    void addShortNotEquals() {
        Short expectedResult = 3;
        short a = 1;
        short b = 1;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(a, b, "Short"));
    }

    @Test
    void addLong() {
        long expectedResult = 2;
        long a = 1;
        long b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "Long"));
    }

    @Test
    void addLongLittleFirstLetterOfType() {
        long expectedResult = 2;
        long a = 1;
        long b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "long"));
    }

    @Test
    void addLongNotEquals() {
        long expectedResult = 3;
        long a = 1;
        long b = 1;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(a, b, "Long"));
    }

    @Test
    void addFloat() {
        float expectedResult = 2.0F;
        float a = 1;
        float b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "Float"));
    }

    @Test
    void addFloatLittleFirstLetterOfType() {
        float expectedResult = 2.0F;
        float a = 1;
        float b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "float"));
    }

    @Test
    void addFloatNotEquals() {
        float expectedResult = 3.0F;
        float a = 1;
        float b = 1;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(a, b, "Float"));
    }

    @Test
    void addDouble() {
        double expectedResult = 2.0;
        double a = 1;
        double b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "double"));
    }

    @Test
    void addDoubleLittleFirstLetterOfType() {
        double expectedResult = 2.0;
        double a = 1;
        double b = 1;
        Assertions.assertEquals(expectedResult, OperationsGeneric.add(a, b, "Double"));
    }

    @Test
    void addDoubleNotEquals() {
        double expectedResult = 3.0;
        double a = 1;
        double b = 1;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.add(a, b, "double"));
    }


    @Test
    void multiplyInt() {
        int expectedResult = 6;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(2, 3, "int"));
    }

    @Test
    void multiplyIntNotEquals() {
        int expectedResult = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(2, 3, "int"));
    }

    @Test
    void multiplyIntLargeFirstLetterOfType() {
        int expectedResult = 4;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(2, 2, "Int"));
    }

    @Test
    void multiplyInteger() {
        int expectedResult = 6;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(2, 3, "Integer"));
    }

    @Test
    void multiplyIntegerNotEquals() {
        int expectedResult = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(2, 3, "Integer"));
    }


    @Test
    void multiplyByte() {
        Byte expectedResult = 6;
        byte a = 2;
        byte b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "Byte"));
    }

    @Test
    void multiplyByteLittleFirstLetterOfType() {
        Byte expectedResult = 6;
        byte a = 2;
        byte b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "byte"));
    }

    @Test
    void multiplyByteNotEquals() {
        Byte expectedResult = 3;
        byte a = 2;
        byte b = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(a, b, "Byte"));
    }

    @Test
    void multiplyShort() {
        Short expectedResult = 6;
        short a = 2;
        short b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "Short"));
    }

    @Test
    void multiplyShortLittleFirstLetterOfType() {
        Short expectedResult = 6;
        short a = 2;
        short b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "short"));
    }

    @Test
    void multiplyShortNotEquals() {
        Short expectedResult = 3;
        short a = 2;
        short b = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(a, b, "Short"));
    }

    @Test
    void multiplyLong() {
        long expectedResult = 6;
        long a = 2;
        short b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "Long"));
    }

    @Test
    void multiplyLongLittleFirstLetterOfType() {
        long expectedResult = 6;
        long a = 2;
        long b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "long"));
    }

    @Test
    void multiplyLongNotEquals() {
        long expectedResult = 3;
        long a = 2;
        long b = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(a, b, "Long"));
    }

    @Test
    void multiplyFloat() {
        float expectedResult = 6.0F;
        float a = 2;
        float b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "Float"));
    }

    @Test
    void multiplyFloatLittleFirstLetterOfType() {
        float expectedResult = 6.0F;
        float a = 2;
        float b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "float"));
    }

    @Test
    void multiplyFloatNotEquals() {
        float expectedResult = 3.0F;
        float a = 2;
        float b = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(a, b, "Float"));
    }

    @Test
    void multiplyDouble() {
        double expectedResult = 6.0;
        double a = 2;
        double b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "Double"));
    }

    @Test
    void multiplyDoubleLittleFirstLetterOfType() {
        double expectedResult = 6.0;
        double a = 2;
        double b = 3;
        Assertions.assertEquals(expectedResult, OperationsGeneric.multiply(a, b, "double"));
    }

    @Test
    void multiplyDoubleNotEquals() {
        double expectedResult = 3.0;
        double a = 2;
        double b = 3;
        Assertions.assertNotEquals(expectedResult, OperationsGeneric.multiply(a, b, "Double"));
    }

    @Test
    void addThrowIllegalArgumentException() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> OperationsGeneric.add(1, 1, "Unsupported_type"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @Test
    void multiplyThrowIllegalArgumentException() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> OperationsGeneric.multiply(1, 1, "Unsupported_type"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

}
