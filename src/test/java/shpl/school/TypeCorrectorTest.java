package shpl.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TypeCorrectorTest {

    @Test
    void getTypeInt() {
        int expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "int"));
    }

    @Test
    void getTypeIntNotEqualsType() {
        double expectedResult = 1;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("1", "int"));
    }

    @Test
    void getTypeInteger() {
        Integer expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "Integer"));
    }

    @Test
    void getTypeIntegerNotEqualsType() {
        double expectedResult = 11;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("11", "Integer"));
    }

    @Test
    void getTypeByte() {
        byte expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "byte"));
    }

    @Test
    void getTypeByteNotEqualsType() {
        double expectedResult = 1;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("1", "byte"));
    }

    @Test
    void getTypeByteLargeTypeName() {
        Byte expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "Byte"));
    }

    @Test
    void getTypeShort() {
        Short expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "short"));
    }

    @Test
    void getTypeShortNotEqualsType() {
        double expectedResult = 1;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("1", "short"));
    }

    @Test
    void getTypeShortLargeTypeName() {
        Short expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "Short"));
    }

    @Test
    void getTypeLong() {
        long expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "long"));
    }

    @Test
    void getTypeLongNotEqualsType() {
        double expectedResult = 1;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("1", "long"));
    }

    @Test
    void getTypeLongLargeTypeName() {
        long expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "Long"));
    }

    @Test
    void getTypeFloat() {
        float expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "float"));
    }

    @Test
    void getTypeFloatNotEqualsType() {
        double expectedResult = 1;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("1", "float"));
    }

    @Test
    void getTypeFloatLargeTypeName() {
        float expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "Float"));
    }

    @Test
    void getTypeDouble() {
        double expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "double"));
    }

    @Test
    void getTypeDoubleNotEqualsType() {
        float expectedResult = 1;
        Assertions.assertNotEquals(expectedResult, TypeCorrector.getType("1", "double"));
    }

    @Test
    void getTypeDoubleLargeTypeName() {
        double expectedResult = 1;
        Assertions.assertEquals(expectedResult, TypeCorrector.getType("1", "Double"));
    }

    @Test
    void getTypeThrowNumberFormatException() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                NumberFormatException.class,
                () -> TypeCorrector.getType("abc", "int"),
                "java.lang.NumberFormatException: Not correct numbers");
        Assertions.assertTrue(thrown.getMessage().contains("Not correct numbers"));
    }

    @Test
    void getTypeThrowIllegalArgumentException() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.getType("1", "Unsupported_type"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }


    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionByteMaxValue() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(Byte.MAX_VALUE + 1), "byte"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionByteMinValue() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(Byte.MIN_VALUE - 1), "byte"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionShortMaxValue() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(Short.MAX_VALUE + 1), "short"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionShortMinValue() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(Short.MIN_VALUE - 1), "short"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionFloatMaxValue() {
        double valueToTest = Double.parseDouble(String.valueOf(Float.MAX_VALUE));
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(valueToTest + 1), "float"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionIntMaxValue() {
        double valueToTest = Integer.MAX_VALUE;
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(valueToTest + 1), "int"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionIntMinValue() {
        double valueToTest = Integer.MIN_VALUE;
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(valueToTest - 1), "int"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionIntegerMaxValue() {
        double valueToTest = Integer.MAX_VALUE;
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(valueToTest + 1), "Integer"));
    }

    @SuppressWarnings("all")
    @Test
    void getTypeThrowCorrectionIntegerMinValue() {
        double valueToTest = Integer.MIN_VALUE;
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> TypeCorrector.correction(String.valueOf(valueToTest - 1), "Integer"),
                "java.lang.IllegalArgumentException: Unsupported type");
        Assertions.assertTrue(thrown.getMessage().contains("Unsupported type"));
    }

}
