package shpl.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

class PropertiesReaderTest {
    private static final String FILE_NAME = "propertyToTest.properties";
    private static final InputStream PATH_TO_PROPERTIES = PropertiesReaderTest.class.getClassLoader().getResourceAsStream(FILE_NAME);

    @Test
    void propertiesReaderCorrectValues() {

        PropertiesReader props = new PropertiesReader(PATH_TO_PROPERTIES);
        Assertions.assertEquals("4", props.getMax());
        Assertions.assertEquals("1", props.getMin());
        Assertions.assertEquals("1", props.getIncrement());
    }

}
