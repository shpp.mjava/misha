package shpl.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class WriteCalculationsTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @SuppressWarnings("unchecked")
    @Test
    <T extends Number> void testWrite() {
        //create array for testing
        T[][] arrayToTest = (T[][]) new Number[2][2];
        T firstNumber = (T) TypeCorrector.getType("1", "int");
        T secondNumber = (T) TypeCorrector.getType("2", "int");
        T thirdNumber = (T) TypeCorrector.getType("2", "int");
        T forthNumber = (T) TypeCorrector.getType("4", "int");
        arrayToTest[0][0] = OperationsGeneric.multiply(firstNumber, firstNumber, "int");
        arrayToTest[0][1] = OperationsGeneric.multiply(secondNumber, firstNumber, "int");
        arrayToTest[1][0] = OperationsGeneric.multiply(thirdNumber, firstNumber, "int");
        arrayToTest[1][1] = OperationsGeneric.multiply(forthNumber, firstNumber, "int");

        // Підготовка виводу
        System.setOut(new PrintStream(outContent));

        // Виклик методу, який потрібно протестувати
        WriteCalculations.write(arrayToTest);

        // Перевірка виводу
        String expectedOutput = "Writing results" + System.lineSeparator()
                + "1 2 " + System.lineSeparator()
                + "2 4 " + System.lineSeparator();
        Assertions.assertEquals(expectedOutput, outContent.toString());

        // Повернення оригінального виводу
        System.setOut(originalOut);
    }

}
