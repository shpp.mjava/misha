package shpl.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculationTest {
    @SuppressWarnings("unchecked")
    @Test
    <T extends Number & Comparable<T>> void calculatePositiveIncrementTest() {
        Calculation solver = new Calculation("1", "2", "1");
        T[][] resultOfCalculations = solver.calculate("int");
        //create array for testing
        T[][] arrayToTest = (T[][]) new Number[2][2];
        T firstNumber = (T) TypeCorrector.getType("1", "int");
        T secondNumber = (T) TypeCorrector.getType("2", "int");
        T thirdNumber = (T) TypeCorrector.getType("2", "int");
        T forthNumber = (T) TypeCorrector.getType("4", "int");
        arrayToTest[0][0] = OperationsGeneric.multiply(firstNumber, firstNumber, "int");
        arrayToTest[0][1] = OperationsGeneric.multiply(secondNumber, firstNumber, "int");
        arrayToTest[1][0] = OperationsGeneric.multiply(thirdNumber, firstNumber, "int");
        arrayToTest[1][1] = OperationsGeneric.multiply(forthNumber, firstNumber, "int");

        Assertions.assertArrayEquals(arrayToTest, resultOfCalculations);
    }

    @SuppressWarnings("unchecked")
    @Test
    <T extends Number & Comparable<T>> void calculateNegativeIncrementTest() {
        Calculation solver = new Calculation("2", "1", "-1");
        T[][] resultOfCalculations = solver.calculate("int");
        //create array for testing
        T[][] arrayToTest = (T[][]) new Number[2][2];
        T firstNumber = (T) TypeCorrector.getType("4", "int");
        T secondNumber = (T) TypeCorrector.getType("2", "int");
        T thirdNumber = (T) TypeCorrector.getType("2", "int");
        T forthNumber = (T) TypeCorrector.getType("1", "int");
        arrayToTest[0][0] = OperationsGeneric.multiply(firstNumber, forthNumber, "int");
        arrayToTest[0][1] = OperationsGeneric.multiply(secondNumber, forthNumber, "int");
        arrayToTest[1][0] = OperationsGeneric.multiply(thirdNumber, forthNumber, "int");
        arrayToTest[1][1] = OperationsGeneric.multiply(forthNumber, forthNumber, "int");

        Assertions.assertArrayEquals(arrayToTest, resultOfCalculations);
    }

    @Test
    void calculateIncrementEqualsZeroTest() {
        Calculation solver = new Calculation("2", "1", "0");
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> solver.calculate("int"));

    }
}
